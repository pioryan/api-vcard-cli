#!/usr/bin/env php
<?php
define('SOURCE', 'https://bytebucket.org/matchmove/api-vcard-cli/raw/master/dist/vcard.phar');
define('HTTP_OK', 200);
define('EXE', 'vcard.phar');

set_exception_handler(function ($e) {
    echo "\n" . $e->getMessage() . "\n";
    exit(1);
});

/**
 * Validation
 */

if (!extension_loaded('curl')) {
    throw new ErrorException('`CUrl` is required. See http://php.net/manual/en/curl.installation.php');
}

$temp = tempnam(sys_get_temp_dir(), 'mmvcardcli' + time());

var_dump($temp);

$fp = fopen($temp, 'w');
$ch = curl_init();

// set URL and other appropriate options
curl_setopt($ch, CURLOPT_URL, SOURCE);
curl_setopt($ch, CURLOPT_VERBOSE, true);
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_FILE, $fp);

// grab URL and pass it to the browser
curl_exec($ch);
$info = curl_getinfo($ch);
$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

// close cURL resource, and free up system resources
curl_close($ch);
fclose($fp);

if (HTTP_OK != $status) {
    throw new ErrorException("Downloading from code was NOT_OK. Status: `$status`");
}

// Archive current copy
if (is_file(EXE)) {
    echo "\nArchiving the current copy.\n";
    $file = pathinfo(EXE);
    $file = $file['filename'] . '_' . time() . round(microtime(true) * 1000) . '.' . $file['extension'];
    copy(EXE, $file);
    chmod($file, 0400);
}

// Applying new copy
echo "\nApplying new copy.\n";
copy($temp, EXE);

// change permission
echo "\nChange permission.\n";
chmod(EXE, 0771);

?>
Installation Successful!

Start using by creating a "vcard.json" file that contains:

{
    "key": "<customer_key>",
    "secret": "<customer_secret>"
}

when done this way, this configuration will be automatically loaded if
the command is run at the same lavel where this file is located.

Alternatively, set environment variables in your .bashrc/.bash_profile
configuration:

export VCARD_CLI_KEY='<customer_key>'
export VCARD_CLI_SECRET='<customer_secret>'

Note: remember to reload the the configuration file by executing
`source .bashrc` or `source .bash_profile` depending on which one is
used.

Other configurations can be set this way are as follows:

"user", VCARD_CLI_USER

    username (email) for oauth authentication.
    Omitting this parameter invokes the request as public

"password", VCARD_CLI_PASSWORD

    password for oauth authentication

"host", VCARD_CLI_HOST

    host domain with protocol to connect to. Default value
    will be [https://api.mmvpay.com/sg/v1] even when it is 
    not set in the JSON configuration file

"ssl-certificate", VCARD_CLI_SSL_CERTIFICATE

    path to the SSL certificate file when connecting to a
    secured connection

For convinience, you can set an alias for the command:

alias vcard='./path/to/vcard.phar'

Run `./vcard.phar --help` for more information.
