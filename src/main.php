<?php
// Define Constants
define('ROOT', class_exists('Phar') && Phar::running() ? Phar::running(): __DIR__);
define('CONFIG_FILE', 'vcard.json');
define('DEFAULT_METHOD', 'GET');
define('DEFAULT_HOST', 'https://api.mmvpay.com/sg/v1');

// Helper for loading files
function _file($path, $ext = 'php') {
    return ROOT . DIRECTORY_SEPARATOR
        . implode(DIRECTORY_SEPARATOR, !is_array($path) ? [$path]: $path) . '.' . $ext;
}

// Pretty errors
set_exception_handler(function ($e) {
    echo "\n" . $e->getMessage() . "\n";
    exit(1);
});

include _file(['vcard', 'matchmovepay']);
MatchMovePay::init();

use MatchMovePay\Helper\Config;
use MatchMovePay\Helper\Option;
use MatchMovePay\Helper\Curl;
use MatchMovePay\VCard\Connection;

$config = new Config('VCARD_CLI_',
    ['host', 'key', 'secret', 'user', 'password', 'ssl-certificate'],
    CONFIG_FILE);

$config = array_merge($config->as_array(),
    Option::get([
        't' => 'host',
        'k' => 'key',
        's' => 'secret',
        'u' => 'user',
        'p' => 'password',
        'c' => 'ssl-certificate'
    ], [
        'h' => 'help',
        'v' => 'verbose'
    ])
 );

if (isset($config['help'])) {
    include _file(['readme'], 'md');
    echo "\n\n";
    exit(0);
}

if (isset($config['verbose'])) {
    Curl::$verbose = true;
}

include _file('validation');

// Start the connection
$conn = new Connection(
    $config['host'],
    $config['key'],
    $config['secret'],
    !empty($config['ssl-certificate']) ? $config['ssl-certificate']: null);

// Authenticate a user
if (!empty($config['user']) && !empty($config['password'])) {
    $conn->authenticate($config['user'], $config['password']);
}

if (!empty($config[2])) {
    parse_str($config[2], $params);
    $config[2] = $params;
}

/**
 * Start consuming resources
 * 0 - method
 * 1 - resource
 * 2 - parameters/query
 */
echo $conn->consume($config[0], $config[1], !empty($config[2]) ? $config[2]: []);