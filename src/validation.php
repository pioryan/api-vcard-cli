<?php
use MatchMovePay\Helper\Exception;

if (empty($config['key'])) {
    throw new Exception('Configuration `key` is undefined.');
}

if (empty($config['secret'])) {
    throw new Exception('Configuration `secret` is undefined.');
}

if (empty($config[0])) {
    throw new Exception('Parameter `method` is undefined. See `--help`.');
}

if (!in_array($config[0], ['POST', 'GET', 'PUT', 'DELETE'])) {
    array_unshift($config, DEFAULT_METHOD);
}

if (empty($config[1])) {
    throw new Exception('Configuration `api` is undefined. See `--help`.');
}

if (empty($config['host'])) {
    $config['host'] = DEFAULT_HOST;
}
