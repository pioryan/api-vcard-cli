Installation
============

To install,

    curl -LsS https://bit.ly/vcardcli | php

or the long way,

    curl -LsS https://bitbucket.org/matchmove/api-vcard-cli/downloads/installer.php | php

Usage
=====

Name
----

vcard.phar - consumes a MatchMove Virtual Card resources.


Synopsis
--------

    php vcard.phar [option]... [method] [api] [query]

Description
-----------

Execute a specific API of MatchMove Virtual Card.

Supported methods are, GET, POST, PUT and DELETE.

The availabe options without the hiphens/dashes can be set
in a JSON file (vcard.json) to be set as the default value
when the option is omitted.

Aside from the JSON file, configurations can also be set
using environment variables. The variable names are the
same as the indexes only they are prefixed with 
(VCARD_CLI_OAUTH_). An example for `key`, will be
`VCARD_CLI_OAUTH_KEY`.

-u email@mail.com, --user=email@mail.com

> username (email) for oauth authentication.
> Omitting this parameter invokes the request as public

-p WORD, --password=WORD

> password for oauth authentication

-k WORD, --key=WORD

> API oauth consumer key

-s WORD, --secret=WORD

> API oauth consumer secret paired to the consumer key

-t https://abc.com/<prd>/<ver>,
--host=https://abc.com/<prd>/<ver>

> host domain with protocol to connect to. Default value
> will be [https://api.mmvpay.com/sg/v1] even when it is 
> not set in the JSON configuration file

-c FILE, --ssl-certificate=FILE

> path to the SSL certificate file when connecting to a
> secured connection

-h, --help

> display this help

Examples
--------

Assume the the vcard.json contains the key and secret
Get the user's details

    $ php vcard.phar -u me@email.com -p $3CuR3 GET users

Create a user

    $ php vcard.phar POST users \
    email=me@email.com&fullname=Some%20Name...

Using default method GET

This example will work when username and password was
provided in vcard.json or the environment variables

    $ php vcard.phar users